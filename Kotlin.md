# Kotlin Cheatsheet

## Basic syntax

### Define packages
```Kotlin
package my.demo

import java.util.*

// Code
```

### Defining functions
```Kotlin
/**
 * Function faving two Int parameters with Int return type
 * @param a: Integer
 * @param b: Integer
 * @return a + b
 */
fun sum(a: Int, b: Int): Int{
    return a + b
}

fun main(args: Array<String>){
    println(sum(3,5)) // 8
}
```

```Kotlin
/**
 * Function with an expression body and inferred return type
 * @param a: Int
 * @param b: Int
 */
fun sum(a: Int, b: Int) = a + b

fun main(args: Array<String>){
    print("sum of 3 and 5 is " ${sum(3,5)}) // 8
}
```

```Kotlin
/**
 * Function returning no meaningful value
 * @param a: Int
 * @param b: Int
 */
fun printSum(a: Int, b: Int): Unit{
    println("sum of $a and $b is ${a + b}")
}

/**
 * Unit return type can be omitted
 * @param a: Int
 * @param b: Int
 */
fun printSum(a: Int, b: Int){
    rintln("sum of $a and $b is ${a + b}")
}

fun main(args: Array<String>){
    printSum(3,5); // sum of 3 and 5 is 8
}
```

### Define varibales

```Kotlin
/**
 * Assign-once (read-only) local variable
 */
val a: Int = 1 // immediate assignment
val b = 2      // `Int` type is inferred 
val c: Int     // Type required with no default value
c = 3          // Deferred assignment

/**
 * Mutable variable
 */
var x = 5 
x += 1

/**
 * Top-level variables
 */
val PI = 3.14
var x = 0

fun incrementX(){
    x += 1
}
```

### Comments
```Kotlin
// End-of-line comments

/* Block comment 
   on multiple line */
```

### String templates
```Kotlin
var a = 1
var s1 = "a is $a"

a = 2
val s2 = "${s1.replace("is", "was")}, but now is $a"
println(s2) // a was 1, but now is 2
```

### Conditional expressions
```Kotlin
/**
 * Create a conditional expression inside a function
 */
fun maxOf(a: Int, b: Int): Int{
    if (a > b){
        return a
    } else {
        return b
    }
}

/**
 * Using if as an expression
 */
fun maxOf(a: Int, b: Int) = if (a > b) a else b
```

### Nullable values and checking for null
```Kotlin
/*
 * Function return null if the parameter does not hold an Integer
 */
fun parseInt(str: String): Int?{
    // code
    return str.toIntOrNull()
}

/**
 * Use a function returning nullable value
 */
fun printProduct(arg1: String, arg2:String){
    val x = parseInt(arg1)
    val y = parseInt(arg2)

    if(x!=null && y!=null){
        println(x*y)
    } else {
        println("either '$arg1' or '$arg2' is not a number")
    }
}
```

### Type checks and automatic casts
```Kotlin
/**
 * A function check if an expression is an instance of a String. If an
 * immutable local variable or property is checked for a specific type,
 * there's no need to cast it explicitly:
 */
fun getStringLength(obj: Any): Int?{
    if(obj is String && obj.length > 0){
        // `obj` is automatically cast to `String` in this branch
        return obj.length
    }
    return null
}
```

### Loop
#### For
```Kotlin
val items = listOf("apple", "banana", "lemon")
for(item in items){
    println(item)
}

// or

for(index in items.indices){
    println("item at $index is ${items[index]}")
}
```
#### While
```Kotlin
val items = listOf("apple", "banana", "lemon")
var index = 0
while(index < items.size){
    println("item at $index is ${items[index]}")
    index++
}
```
### When expression
```Kotlin
fun describe(obj: Any): String = 
when(obj){
    1 -> "One"
    "Hello" -> "Greeting"
    is Long -> "Long"
    !is String -> "Not a String"
    else -> "Unknwon"
}

fun main(args: Array<String>){
    println(describe(1)) // One
    println(describe("Hello")) // Greeting
    println(describe(1000L)) // Long
    println(describe(2)) // Not a String
    println(describe("other")) // Unknown
}
```

### Range
```Kotlin
/**
 * Check if a number is whithin a range using in operator
 */
fun checkInRange(){
    val x = 10
    val y = 9
    if(x in 1..y+1){
        println("fits in range")
    }
}

/**
 * Check if a bumber is out of range
 */
fun checkOutOfRange(){
    val list = listOf("a", "b", "c")
    if(-1 !in 0..list.lastIndex) println("-1 is out of range")
    if(list.size !in list.indices){
        println("list size is out of calid list indices range too")
    }
}
```

```Kotlin
// Iterating over a range
for(x in 1..5){
    print(x) // 12345
} 

// or over a progression
for(x in 1..10 step 2){
    print(x) // 13579
}

for(x in 9 downTo 0 step 3){
    print(x) // 9630
}
```

### Collection
```Kotlin
// Iterating over a collection
for(item in items){
    println(item)
}

// Checking if a collection contains an object using in operator
when{
    "orange" in items -> println("juicy")
    "apple" in items -> println("apple is fine too")
}

// Using lambda expressions to filter and map collections
val fruits = listOf("banana", "avocado", "apple", "kiwi")
fruits.filter {
    it.startsWith("a")
}.sortedBy{
    it
}.map{
    it.toUppperCase()
}.forEach{
    println(it)
}
```

### Basic classes and their instances

```Kotlin
abstract class Shape(val sides: List<Double>){
    val perimeter: Double get() = sides.sum()
    abstract fun calculateArea(): Double
}

interface RectangleProperties{
    val isSquare: Boolean
}

class Rectangle(
    var height: Double,
    var length: Double
) : Shape(listOf(height, length, height, length)), RectangleProperties{
    override val isSquare: Boolean get() = length == height
    override fun calculateArea(): Double = height * length
}

class Triangle(
    var sideA: Double,
    var sideB: Double,
    var sideC: Double
) : Shape(listOf(sideA, sideB, sideC)){
    override fun calculateArea(): Double {
        val s = perimeter / 2
        return Math.sqrt(s*(s-sideA)*(s-sideB)*(s-sideC))
    }
}

fun main(args: Array<String>){
    val rectangle = Rectangle(4.0, 3.0) // No new in instance
    val triangle = Triangle(3.0, 4.0, 5.0)
    println("Area of rectangle is ${rectangle.calculateArea(), its perimeter is ${rectangle.perimeter}}") // 12.0 and 14.0
}
```

